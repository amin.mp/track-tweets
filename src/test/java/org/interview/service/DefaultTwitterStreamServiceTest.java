package org.interview.service;

import org.interview.model.Message;
import org.interview.model.User;
import org.interview.store.MessageStore;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.PrintStream;

import static org.mockito.Matchers.*;

/**
 * @author <a href="amin.malekpour@hotmail.com">Amin Malekpour</a>
 * @version 1, 30 Mar 2019
 */
@RunWith(SpringRunner.class)
public class DefaultTwitterStreamServiceTest {

    @InjectMocks
    DefaultTwitterStreamService service;

    @Mock
    PrintStream printStream;

    @Mock
    MessageStore messageStore;

    private Message msg1User1 = new Message(1L, null, null, new User(1L, null, null, null));
    private Message msg2User1 = new Message(2L, null, null, msg1User1.getAuthor());
    private Message msg3User2 = new Message(3L, null, null, new User(2L, null, null, null));
    private Message msg4User3 = new Message(4L, null, null, new User(3L, null, null, null));

    @Before
    public void before() {
        service.saveMessage(msg1User1);
        service.saveMessage(msg2User1);
        service.saveMessage(msg3User2);
        service.saveMessage(msg4User3);
    }

    @Test
    public void handleIncomingMessages() {
        Mockito.verify(messageStore, Mockito.times(2)).add(eq(msg1User1.getAuthor()), any(Message.class));
        Mockito.verify(messageStore).add(eq(msg3User2.getAuthor()), any(Message.class));
        Mockito.verify(messageStore).add(eq(msg4User3.getAuthor()), any(Message.class));
    }

    @Test(expected = IllegalArgumentException.class)
    public void printReportExc() {
        service.printReport();
    }

    @Test
    public void printReport() {
        service.streamingStarted();
        service.streamingFinished();
        service.printReport();
        Mockito.verify(printStream, Mockito.atLeast(3)).println(anyString());
    }
}
