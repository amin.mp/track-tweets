package org.interview.oauth;

import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;
import twitter4j.TwitterStream;

@Profile("test")
@Component
public class TestTwitterStreamInitializer implements TwitterStreamInitializer {
    @Override
    public void initialize(TwitterStream twitterStream) {}
}
