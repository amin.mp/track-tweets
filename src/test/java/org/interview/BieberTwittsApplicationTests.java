package org.interview;

import org.interview.job.TwitterStreamJob;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * @author <a href="amin.malekpour@hotmail.com">Amin Malekpour</a>
 * @version 1, 30 Mar 2019
 */
@RunWith(SpringRunner.class)
@SpringBootTest
@ActiveProfiles("test")
public class BieberTwittsApplicationTests {

    @Autowired
    private TwitterStreamJob twitterStreamJob;

    @Test
    public void contextLoads() {
        Assert.assertNotNull(twitterStreamJob);
    }
}
