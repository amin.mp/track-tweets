package org.interview.store;

import org.apache.commons.lang3.time.DateUtils;
import org.interview.model.Message;
import org.interview.model.User;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Arrays;
import java.util.Date;

/**
 * @author <a href="amin.malekpour@hotmail.com">Amin Malekpour</a>
 * @version 1, 30 Mar 2019
 */
@RunWith(SpringRunner.class)
public class DefaultMessageStoreTest {

    MessageStore store;

    private Date yesterday = DateUtils.addDays(new Date(), -1);
    private Date today = new Date();
    private Date tomorrow = DateUtils.addDays(new Date(), 1);

    private Message msg1User1 = new Message(1L, today, null, new User(1L, today, null, null));
    private Message msg2User1 = new Message(2L, tomorrow, null, msg1User1.getAuthor());
    private Message msg3User2 = new Message(3L, tomorrow, null, new User(2L, yesterday, null, null));
    private Message msg4User3 = new Message(4L, null, null, new User(3L, tomorrow, null, null));

    @Before
    public void before() {
        store = new DefaultMessageStore();
        store.add(msg1User1.getAuthor(), msg1User1);
        store.add(msg2User1.getAuthor(), msg2User1);
        store.add(msg3User2.getAuthor(), msg3User2);
        store.add(msg4User3.getAuthor(), msg4User3);
    }

    @Test
    public void getUsers_whenThereAre3Users_thenReturn3Users() {
        Assert.assertEquals(Arrays.asList(msg3User2.getAuthor(), msg1User1.getAuthor(), msg4User3.getAuthor()), store.getUsers());
    }

    @Test
    public void getMessages_whenUserIdIs1_thenReturnMessagesForUser1() {
        Assert.assertEquals(Arrays.asList(msg1User1, msg2User1), store.getMessages(msg1User1.getAuthor()));
    }
}
