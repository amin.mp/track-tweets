package org.interview.model;

import java.util.Date;

/**
 * @author <a href="amin.malekpour@hotmail.com">Amin Malekpour</a>
 * @version 1, 30 Mar 2019
 */
public class Message extends BaseModel {

    private String text;
    private User author;

    public Message(Long id, Date creationDate, String text, User author) {
        super(id, creationDate);
        this.text = text;
        this.author = author;
    }

    public String getText() {
        return text;
    }

    public User getAuthor() {
        return author;
    }

    @Override
    public String toString() {
        return "Message{" +
                "author=" + author +
                '}';
    }
}
