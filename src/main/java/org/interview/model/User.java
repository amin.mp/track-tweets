package org.interview.model;

import java.util.Date;

/**
 * @author <a href="amin.malekpour@hotmail.com">Amin Malekpour</a>
 * @version 1, 30 Mar 2019
 */
public class User  extends BaseModel {

    private String username;
    private String screenName;

    public User(Long id, Date creationDate, String username, String screenName) {
        super(id, creationDate);
        this.username = username;
        this.screenName = screenName;
    }

    public String getUsername() {
        return username;
    }

    public String getScreenName() {
        return screenName;
    }

    @Override
    public String toString() {
        return "User{" +
                "id=" + super.getId() +
                ", creationDate=" + super.getCreationDate() +
                ", username='" + username + '\'' +
                ", screenName='" + screenName + '\'' +
                '}';
    }
}
