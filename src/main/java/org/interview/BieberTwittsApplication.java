package org.interview;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.scheduling.annotation.EnableScheduling;

import java.io.InputStream;
import java.io.PrintStream;

/**
 * @author <a href="amin.malekpour@hotmail.com">Amin Malekpour</a>
 * @version 1, 30 Mar 2019
 */
@SpringBootApplication
@EnableScheduling
public class BieberTwittsApplication {
    public static void main(String[] args) {
        SpringApplication.run(BieberTwittsApplication.class, args);
    }

    @Bean
    public PrintStream userPrintStream() {
        return System.out;
    }

    @Bean
    public InputStream userInputStream() {
        return System.in;
    }
}
