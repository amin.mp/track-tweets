package org.interview.service;

import org.apache.commons.lang3.time.DateFormatUtils;
import org.interview.model.Message;
import org.interview.model.User;
import org.interview.store.MessageStore;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import java.io.PrintStream;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * @author <a href="amin.malekpour@hotmail.com">Amin Malekpour</a>
 * @version 1, 30 Mar 2019
 */
@Service
public class DefaultTwitterStreamService implements TwitterStreamService {

    @Autowired
    private PrintStream printStream;

    @Value("${twitts.track}")
    private String track;

    @Autowired
    private MessageStore store;

    private Date startTime;
    private Date endTime;

    @Override
    public void streamingStarted() {
        startTime = new Date();
    }

    @Override
    public void streamingFinished() {
        endTime = new Date();
    }


    @Override
    public void saveMessage(final Message message) {
        store.add(message.getAuthor(), message);
    }

    @Override
    public void printReport() {
        Assert.notNull(startTime, "print was called before start of streaming");
        Assert.notNull(endTime, "print was called before end of streaming");
        printStream.println();
        printStream.println("----------<Generating Report>----------");
        printSummary();
        printDetail();
    }

    private void printDetail() {
        printStream.println();
        printStream.println("----------<Detail>----------");
        store.getUsers().forEach(user -> sortThenPrintMessages(user, store.getMessages(user)));
    }

    private void printSummary() {
        printStream.println();
        printStream.println("----------<Summary>----------");
        AtomicInteger count = new AtomicInteger(0);
        store.getUsers().forEach(user -> count.addAndGet(store.getMessages(user).size()));
        printStream.println(String.format("Streaming twitter track on '%s', from (%s) till (%s), " +
                        "receiving %s number of twitts from %s number of users", track,
                DateFormatUtils.ISO_8601_EXTENDED_DATETIME_FORMAT.format(startTime),
                DateFormatUtils.ISO_8601_EXTENDED_DATETIME_FORMAT.format(endTime), count.get(), store.getUsers().size()));
    }

    private void sortThenPrintMessages(User user, List<Message> messageList) {
        Collections.sort(messageList, Comparator.comparing(Message::getCreationDate));
        printStream.println("----------<Author>----------");
        printObject(user);
        for (int i = 0; i < messageList.size(); i++) {
            printStream.println(String.format("----------<Message(%s)>----------", i + 1));
            printObject(messageList.get(i));
        }
        printStream.println();
    }

    private void printObject(Object message) {
        printStream.println(message);
    }
}
