package org.interview.service;

import org.interview.model.Message;

/**
 * @author <a href="amin.malekpour@hotmail.com">Amin Malekpour</a>
 * @version 1, 30 Mar 2019
 */
public interface TwitterStreamService {

    /**
     * Call once when streaming is started.
     */
    void streamingStarted();

    /**
     * Call once when streaming is finished.
     */
    void streamingFinished();

    /**
     * Save new message.
     * @param message
     */
    void saveMessage(Message message);

    /**
     * Print saved messages group by authors in ascending order of creationDate(for both author and message).
     */
    void printReport();
}
