package org.interview.oauth;


import twitter4j.TwitterStream;

/**
 * @author <a href="amin.malekpour@hotmail.com">Amin Malekpour</a>
 * @version 1, 30 Mar 2019
 */
public interface TwitterStreamInitializer {

    void initialize(TwitterStream twitterStream);
}
