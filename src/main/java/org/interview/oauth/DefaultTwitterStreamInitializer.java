package org.interview.oauth;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;
import twitter4j.HttpResponseCode;
import twitter4j.TwitterException;
import twitter4j.TwitterStream;
import twitter4j.auth.AccessToken;
import twitter4j.auth.RequestToken;

import java.io.InputStream;
import java.io.PrintStream;
import java.util.Scanner;

/**
 * @author <a href="amin.malekpour@hotmail.com">Amin Malekpour</a>
 * @version 1, 30 Mar 2019
 */
@Profile("!test")
@Component
public class DefaultTwitterStreamInitializer implements TwitterStreamInitializer {

    private final Logger logger = LoggerFactory.getLogger(DefaultTwitterStreamInitializer.class);

    @Value("${consumer.key}")
    private String consumerKey;
    @Value("${consumer.secret}")
    private String consumerSecret;

    @Autowired
    private PrintStream printStream;
    @Autowired
    private InputStream inputStream;

    @Override
    public void initialize(final TwitterStream twitterStream) {
        twitterStream.setOAuthConsumer(consumerKey, consumerSecret);
        AccessToken accessToken = null;
        RequestToken requestToken;

        try {
            requestToken = twitterStream.getOAuthRequestToken();
        } catch (TwitterException e) {
            logger.error("Failed to getting OAuth request token.", e);
            return;
        }

        while (accessToken == null) {
            printStream.println("Open the following URL and give access to your account:");
            printStream.println(requestToken.getAuthorizationURL());
            printStream.println("Enter the available PIN or just hit enter.[PIN]:");
            String pin = new Scanner(inputStream).next();
            try {
                if (pin.length() > 0) {
                    accessToken = twitterStream.getOAuthAccessToken(requestToken, pin);
                } else {
                    accessToken = twitterStream.getOAuthAccessToken();
                }
            } catch (TwitterException e) {
                if (HttpResponseCode.UNAUTHORIZED == e.getStatusCode()) {
                    logger.error("Unable to get the access token because of the http code 401 (unauthorized).");
                } else {
                    logger.error("Unable to get the access token because of an Exception", e);
                }
            }
        }
    }
}
