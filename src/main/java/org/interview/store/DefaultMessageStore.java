package org.interview.store;

import org.interview.model.Message;
import org.interview.model.User;
import org.springframework.stereotype.Component;

import java.util.*;
import java.util.stream.Collectors;

/**
 * @author <a href="amin.malekpour@hotmail.com">Amin Malekpour</a>
 * @version 1, 30 Mar 2019
 */
@Component
public class DefaultMessageStore implements MessageStore {

    private Map<User, List<Message>> store;

    public DefaultMessageStore() {
        store = new HashMap<>();
    }

    @Override
    public synchronized void add(User user, Message message) {
        if (!store.containsKey(user)) {
            store.put(user, new ArrayList<>());
        }
        store.get(user).add(message);
    }

    @Override
    public List<User> getUsers() {
        return store.keySet().stream().sorted(Comparator.comparing(User::getCreationDate)).collect(Collectors.toList());
    }

    @Override
    public List<Message> getMessages(User user) {
        return store.get(user);
    }
}
