package org.interview.store;

import org.interview.model.Message;
import org.interview.model.User;

import java.util.List;

/**
 * @author <a href="amin.malekpour@hotmail.com">Amin Malekpour</a>
 * @version 1, 30 Mar 2019
 */
public interface MessageStore {

    /**
     * Add user and corresponding message
     * @param user
     * @param message
     */
    void add(User user, Message message);

    /**
     * Get all stored users
     * @return
     */
    List<User> getUsers();

    /**
     * Get all messages of a specific user
     * @param user
     * @return
     */
    List<Message> getMessages(User user);
}
