package org.interview.job;

import org.interview.oauth.TwitterStreamInitializer;
import org.interview.service.TwitterStreamService;
import org.interview.model.Message;
import org.interview.model.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import twitter4j.*;

import java.io.PrintStream;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

/**
 * @author <a href="amin.malekpour@hotmail.com">Amin Malekpour</a>
 * @version 1, 30 Mar 2019
 */
@Component
public class TwitterStreamJob extends StatusAdapter {

    private final Logger logger = LoggerFactory.getLogger(TwitterStreamJob.class);

    @Value("${twitts.track}")
    private String track;
    @Value("${max.message}")
    private int maxMessage;
    @Value("${streaming.time}")
    private int streamingTime;

    @Autowired
    private PrintStream printStream;
    @Autowired
    private TwitterStreamService twitterStreamService;
    @Autowired
    private TwitterStreamInitializer twitterStreamInitializer;

    private TwitterStream twitterStream;
    private ScheduledExecutorService executor;
    private int msgCounter = 0;

    public TwitterStreamJob() {
        twitterStream = new TwitterStreamFactory().getInstance();
        executor = Executors.newSingleThreadScheduledExecutor();
    }

    @Scheduled(initialDelay = 500, fixedRate = Long.MAX_VALUE)
    public void execute() {
        twitterStreamInitializer.initialize(twitterStream);
        printStream.println(String.format("----------<Waiting maximum of %s seconds>----------", streamingTime));
        printStream.println("Streaming..");
        twitterStream.addListener(this);
        twitterStream.filter(new FilterQuery().track(track));
        twitterStreamService.streamingStarted();
        executor.scheduleAtFixedRate(() -> printStream.print('.'), 0L, 1L, TimeUnit.SECONDS);
        executor.schedule(() -> printReportAndShutDown(), streamingTime, TimeUnit.SECONDS);
    }

    @Override
    public void onStatus(Status status) {
        msgCounter++;
        if (msgCounter >= maxMessage) {
            printReportAndShutDown();
        } else {
            final User author = new User(status.getUser().getId(), status.getUser().getCreatedAt(), status.getUser().getName(), status.getUser().getScreenName());
            final Message message = new Message(status.getId(), status.getCreatedAt(), status.getText(), author);
            twitterStreamService.saveMessage(message);
        }
    }

    @Override
    public void onException(Exception e) {
        logger.error("Exception in twitter status listener.", e);
    }

    private void printReportAndShutDown() {
        twitterStreamService.streamingFinished();
        twitterStream.cleanUp();
        twitterStreamService.printReport();
        executor.shutdownNow();
        System.exit(0);
    }
}
